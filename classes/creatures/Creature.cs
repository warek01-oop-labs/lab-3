using OOPLab3.classes.abstract_classes;

namespace OOPLab3.classes.creatures;

public class Creature : Movable {
	private double _height;
	private double _weight;

	public double Height {
		get => _height;
		protected set {
			if (value < 0)
				throw new Exception("Height can't be negative");
			_height = value;
		}
	}

	public double Weight {
		get => _weight;
		protected set {
			if (value < 0)
				throw new Exception("Weight can't be negative");
			_weight = value;
		}
	}

	public DateTime BirthDate { get; protected set; }

	public Creature(double defaultWeight, double defaultHeight, DateTime? bdate) {
		BirthDate = bdate.GetValueOrDefault(DateTime.Now);
		Weight    = defaultHeight;
		Height    = defaultHeight;
	}
}
