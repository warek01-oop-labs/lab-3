namespace OOPLab3.classes.creatures.human;

public class Human : Creature {
	private string _gender    = "Male";
	private string _job       = "Unemployed";
	private bool   _isWalking = false;

	public string FirstName { get; }

	public string LastName { get; }

	public string FullName {
		get => $"{FirstName} {LastName}";
	}

	public string Gender {
		get => _gender;
		init => _gender = value;
	}

	public string Job {
		get => _job;
		private set => _job = value;
	}

	public Human(string fname, string lname, DateTime bdate)
		: base(70, 170, bdate) {
		FirstName = fname;
		LastName  = lname;
	}

	public void Walk() {
		if (_isWalking) {
			Console.WriteLine($"{FullName} is already walking");
			return;
		}

		Console.WriteLine($"{FullName} is walking");
		_isWalking = true;
	}

	public void Stop() {
		_isWalking = false;
	}

	public void Present() {
		Console.WriteLine($"{FullName} is a {Gender}, {Weight} kg and {Height} cm height");
	}

	public void Eat() {
		Weight++;
	}

	public void Run() {
		Weight--;
	}

	public void Grow() {
		Height++;
	}

	public void ChangeJob(string job) {
		Job = job;
		Console.WriteLine($"{FullName} has changed job to {Job}");
	}
}
