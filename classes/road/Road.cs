using OOPLab3.classes.abstract_classes;

namespace OOPLab3.classes.road;

public class Road : Unmovable {
	private static readonly string[] AsphaltTypes = {
		"Porous", "Perpetual Pavement", "Quiet Pavement", "Warm-Mix", "Thin Overlays"
	};

	private static readonly string[] RoadTypes = {
		"National", "Boulevard", "Off Road", "Basic", "Highway", "Tunnel", "Street", "Side Road", "Avenue"
	};

	private readonly string _type = "Basic";

	private readonly string _asphaltType = "Porous";

	public int BandsNumber { get; init; } = 2;

	public int SpeedLimit { get; init; } = 50;

	public string Name { get; }

	public string AsphaltType {
		get => _asphaltType;
		init {
			if (AsphaltTypes.Contains(value))
				throw new Exception($"Unknown asphalt type {value}");
			_asphaltType = value;
		}
	}

	public int AsphaltHeight { get; init; }

	public string Type {
		get => _type;
		init {
			if (RoadTypes.Contains(value))
				throw new Exception($"Unknown road type {value}");

			_type = value;
		}
	}

	public Road(string name, string asphaltType = "Porous", string roadType = "National",
	            int    asphaltHeight = 10) {
		if (!AsphaltTypes.Contains(asphaltType)) {
			throw new Exception($"Unknown asphalt type {asphaltType}");
		}

		if (asphaltHeight <= 0) {
			throw new Exception("Asphalt height can't be negative");
		}

		Name          = name;
		AsphaltType   = asphaltType;
		AsphaltHeight = asphaltHeight;
		Type          = roadType;
	}
}
