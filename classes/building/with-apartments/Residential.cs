namespace OOPLab3.classes.building.with_apartments;

public class Residential : Building {
	private readonly int[,] _apartmentsArr;
	private readonly int    _apartmentsPerFloor;

	private int _totalResidents = 0;

	public int ResidentsCount => _totalResidents;

	public int Floors => _floorsCount;

	public int ApartmentsCount { get; }

	public double ApartmentArea => _floorArea / _apartmentsPerFloor;

	public double AverageAreaPerResident => _floorArea * 8 / ResidentsCount;

	public Residential(int width, int height, int floors, int apartments, DateTime buildDate)
		: base("Residential", width, height, floors, buildDate) {
		if (apartments % floors != 0)
			throw new Exception("Apartments count must be a multiplier of floors");

		ApartmentsCount     = apartments;
		_apartmentsPerFloor = apartments / floors;
		_apartmentsArr      = new int[floors, _apartmentsPerFloor];
	}

	public void SettlePersons(int apartment, int count) {
		_apartmentsArr[
			GetFloorOfApartment(apartment),
			GetApartmentIndexOnFloor(apartment)
		] += count;
		_totalResidents += count;
	}

	public void UnsettlePersons(int apartment, int count) {
		int floor = GetFloorOfApartment(apartment);
		int index = GetApartmentIndexOnFloor(apartment);

		if (_apartmentsArr[floor, index] == 0)
			throw new Exception("That apartment is empty");

		if (_apartmentsArr[floor, index] < count)
			throw new Exception("Not that many people are living here");

		_apartmentsArr[floor, index] -= count;
		_totalResidents              -= count;
	}

	public int GetPersonsCountInApartment(int apartment) {
		return _apartmentsArr[
			GetFloorOfApartment(apartment),
			GetApartmentIndexOnFloor(apartment)
		];
	}

	public int GetFloorOfApartment(int apartment) {
		return apartment / Floors;
	}

	public int GetApartmentIndexOnFloor(int apartment) {
		return apartment % _apartmentsPerFloor;
	}
}
