using OOPLab3.classes.abstract_classes;

namespace OOPLab3.classes.building;

public class Building : Unmovable {
	protected int _floorArea;
	protected int _floorsCount;

	public string Type { get; }

	public int Height { get; }

	public int Width { get; }

	public DateTime BuildDate { get; }

	public Building(string type, int width, int height, int floors, DateTime buildDate) {
		BuildDate    = buildDate;
		Type         = type;
		Width        = width;
		Height       = height;
		_floorsCount = floors;
		_floorArea   = width * height;
	}
}
