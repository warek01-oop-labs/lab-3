namespace OOPLab3.classes.vehicles.bicicles;

public class StreetBike : Bicicle {
	public StreetBike(string brand, string model)
		: base(brand, model, "Street") { }
}
