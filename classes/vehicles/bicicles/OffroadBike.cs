namespace OOPLab3.classes.vehicles.bicicles;

public class OffroadBike : Bicicle {
	public OffroadBike(string brand, string model)
		: base(brand, model, "Offroad") { }
}
