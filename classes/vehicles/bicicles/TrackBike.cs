namespace OOPLab3.classes.vehicles.bicicles;

public class TrackBike : Bicicle {
	public TrackBike(string brand, string model)
		: base(brand, model, "Track") { }
}
