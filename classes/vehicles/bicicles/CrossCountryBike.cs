namespace OOPLab3.classes.vehicles.bicicles;

public class CrossCountryBike : Bicicle {
	public CrossCountryBike(string brand, string model)
		: base(brand, model, "Cross-country") { }
}
