namespace OOPLab3.classes.vehicles.bicicles;

public class RacingBike : Bicicle {
	public RacingBike(string brand, string model)
		: base(brand, model, "Racing") { }
}
