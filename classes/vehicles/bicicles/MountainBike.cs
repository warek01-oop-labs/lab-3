namespace OOPLab3.classes.vehicles.bicicles;

public class MountainBike : Bicicle {
	public MountainBike(string brand, string model)
		: base(brand, model, "Mountain") { }
}
