namespace OOPLab3.classes.vehicles.cars;

public class Car : Vehicle {
	private readonly string _carType;
	private readonly int    _maxSpeed  = 200;
	private          bool   _isLocked  = true;
	private          bool   _isDriving = false;
	private          string _carName;

	public int MaxSpeed {
		get => _maxSpeed;
		init {
			if (value < 0) throw new Exception("Speed can't me negative");
			_maxSpeed = value;
		}
	}

	public double Height    { get; init; }
	public double Width     { get; init; }
	public double Clearance { get; init; }

	public string CarInfo => $"{_carName} has {DoorsCount} doors, {Weight} kg, max speed of {MaxSpeed} km/h";

	public string Position => $"X:{X}, Y:{Y}";

	public Car(int doorsCount, string type, string carName)
		: base(doorsCount, 4, "Car", 1200) {
		_carType = type;
		_carName = carName;
	}

	public void Drive() {
		Console.WriteLine(_carName + (
			!_isLocked
				? " is driving"
				: " can't drive, it's locked"
		));

		if (!_isLocked) {
			_isDriving = true;
		}
	}

	public void DriveTo(double x, double y) {
		if (_isLocked) {
			Console.WriteLine($"{_carName} is locked");
			return;
		}

		Console.WriteLine($"{_carName} is driving to {x}, {y} ...");
		Console.WriteLine($"{_carName} has arrived to to {x}, {y}");
		ChangePosition(x, y);
		_isDriving = false;
	}

	public void StopDriving() {
		Console.WriteLine(_carName + (
				_isDriving
					? " has stopped"
					: " is already stopped"
			)
		);

		_isDriving = false;
	}

	public void Lock() {
		_isLocked = true;
	}

	public void Unlock() {
		_isLocked = false;
	}
}
