namespace OOPLab3.classes.vehicles.cars;

public class Hatchback : Car {
	public Hatchback(string brandName, bool isShort) : base(isShort ? 2 : 4, "Hatchback", brandName) {
		
	}
}
