using OOPLab3.classes.abstract_classes;

namespace OOPLab3.classes.vehicles;

public abstract class Vehicle : Movable {
	private int    _doorsCount;
	private int    _wheelsCount;
	private string _type;
	private double _weight;

	public int    DoorsCount  => _doorsCount;
	public int    WheelsCount => _wheelsCount;
	public string VehicleInfo => $"{_type} has {_doorsCount} doors and {_wheelsCount} wheels";

	public double Weight {
		get => _weight;
		init {
			if (value < 0)
				throw new Exception("Weight can't be negative");

			_weight = value;
		}
	}

	protected Vehicle(int doorsCount, int wheelsCount, string type, double defaultWeight) {
		if (doorsCount < 0)
			throw new Exception("Doors count can't be negative");

		if (wheelsCount < 0)
			throw new Exception("Wheels count can't be negative");
		
		if (defaultWeight < 0)
			throw new Exception("Weight can't be negative");

		_doorsCount  = doorsCount;
		_wheelsCount = wheelsCount;
		_type        = type;
		_weight      = defaultWeight;
	}
}
