namespace OOPLab3.classes.abstract_classes;

public abstract class Movable : Entity {
	public double X => _x;
	public double Y => _y;

	protected void ChangePosition(double x, double y) {
		_x = x;
		_y = y;
	}
}
